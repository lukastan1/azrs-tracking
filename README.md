# azrs-tracking

## Opis projekta

Ovo je projekat iz predmeta [Alati za razvoj softvera](https://gitlab.com/matf-bg-ac-rs/course-azrs/MATF-AZRS).

## Kanban tabla

Kanban tabla se nalazi na [ovom linku](https://gitlab.com/lukastan1/azrs-tracking/-/boards).

## Korišćeni alati:

[Git](https://git-scm.com/)

[GDB](https://www.sourceware.org/gdb/)

[CI u GitLab-u](https://docs.gitlab.com/ee/ci/)

[git hook-ovi](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks)

[CMake](https://cmake.org/)

[Valgrind](https://valgrind.org/)

[clang-tidy](https://releases.llvm.org/6.0.1/tools/clang/tools/extra/docs/clang-tidy/index.html)

[netcat](https://netcat.sourceforge.net/)
